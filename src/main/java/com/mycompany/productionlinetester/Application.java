/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.productionlinetester;

import java.util.Arrays;
import java.util.stream.Stream;

/**
 *
 * @author rembareeg
 */
public class Application {
    
    public static void main(String[] args) {
        Stream<Product> productStream = Arrays.stream(ProductBuilder.buildRandomProducts(100));
        ProductionLineTester tester = new ProductionLineTester();
        ProductLineTestReport report = tester.test(productStream);
        System.out.println(report);
    }
}
