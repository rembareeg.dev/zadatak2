/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.productionlinetester;

/**
 *
 * @author rembareeg
 */
class ProductLineTestReport {

    long correctCnt; // number of correct products
    long checkedExcCnt; // number of products which threw a checked exception during verification
    long uncheckedExcCnt; // number of products which threw an unchecked exception during verification
    long otherExcCnt; // number of products which threw an error during verification

    ProductLineTestReport(long correctCnt, long checkedExcCnt, long uncheckedExcCnt, long otherExcCnt) {
        this.correctCnt = correctCnt;
        this.checkedExcCnt = checkedExcCnt;
        this.uncheckedExcCnt = uncheckedExcCnt;
        this.otherExcCnt = otherExcCnt;
    }
    
    @Override
    public String toString() {
        return "Number of correct products: " + correctCnt + "\nNumber of product which threw a checked exception: " + checkedExcCnt +
                "\nNumber of produch wichthrew an unchecked exception: " + uncheckedExcCnt + "\nNumber of product which therw an error: " + otherExcCnt;
    }
}