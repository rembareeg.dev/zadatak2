/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.productionlinetester;

import java.util.Random;

/**
 *
 * @author rembareeg
 */
public class ProductBuilder {
        
    public static Product[] buildRandomProducts(int amount){
        Product[] products = new Product[amount - 1];
        
        Random rand = new Random();

        for(int i = 0; i < products.length; i++) {
            int productType = rand.nextInt(6);
            Product p;
            switch (productType) {
                case 0: {
                    p = null;
                    break;
                }
                case 1: {
                    p = new Product();
                    p.setStatus("ok");
                    break;
                }
                case 2: {
                    p = new Product();
                    p.setStatus("InValiD");
                    break;
                }
                case 3: {
                    p = new Product();
                    p.setStatus("checked");
                    break;
                }
                case 4: {
                    p = new Product();
                    p.setStatus("unchecked");
                    break;
                }
                case 5: {
                    p = new Product();
                    p.setStatus("error");
                    break;
                }
                default: {
                    p = null;
                }
            }
            products[i] = p;
        }
        
        return products;
    }
}
