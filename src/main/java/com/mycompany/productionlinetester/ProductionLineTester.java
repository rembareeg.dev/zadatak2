/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.productionlinetester;

import java.util.stream.Stream;

/**
 *
 * @author rembareeg
 */
public class ProductionLineTester {
    
    private final ProductVerifier productVerifierInstance;
    
    private int correctCnt;
    private int checkedExcCnt;
    private int uncheckedExcCnt;
    private int otherExcCnt;

    public ProductionLineTester() {
        this.productVerifierInstance = new ProductVerifierConcrete();
    }
    
    public ProductLineTestReport test(Stream<Product> products){        
        if(products == null) {
            return new ProductLineTestReport(0, 0, 0, 0);
        }
        correctCnt = checkedExcCnt = uncheckedExcCnt = otherExcCnt = 0;        
        products.filter(p -> p != null)
                .filter(p -> !p.getStatus().toLowerCase().equalsIgnoreCase("invalid"))
                .skip(10)
                .limit(20)
                .forEach(p -> {
                    try{
                        productVerifierInstance.verify(p);
                        correctCnt++;
                    }
                    catch(RuntimeException e) {
                        uncheckedExcCnt++;
                    }
                    catch(Exception e) {
                        checkedExcCnt++;
                    }
                    catch(Error e) {
                        otherExcCnt++;
                    } 
                    
                });
     
        
        return new ProductLineTestReport(correctCnt, uncheckedExcCnt, checkedExcCnt, otherExcCnt);
    }
        
        
    
    
    
    
}
