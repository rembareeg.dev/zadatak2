/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.productionlinetester;

/**
 *
 * @author rembareeg
 */
public class Product implements java.io.Serializable{
    
    private String status; 

    public Product() {
    }
    
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
